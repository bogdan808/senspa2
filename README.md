# Стартовый проект с gulp


## Запуск проекта

1. Клонировать этот репозиторий в новую папку (`git clone git@bitbucket.org:bogdan808/senspa2.git`)
2. Установите node.js версии 8.17.0. Сборщик писался давненько. nvm в помощь )
3. `npm i`
4. `gulp`

## Страницы проекта
http://localhost:8083/typo.html
http://localhost:8083/index.html
http://localhost:8083/contacts.html
http://localhost:8083/service.html
http://localhost:8083/services.html
http://localhost:8083/servicesall.html
http://localhost:8083/store.html


В проекте используется препроцессор pug для html.

