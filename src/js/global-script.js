
$(function() {

    $('.beautifulbutton').hover(function() {
        $(this).removeClass('hoverout');
        $(this).addClass('hover');
        $('.fs__btnhover').removeClass('hoverout');
        $('.fs__btnhover').addClass('hover');
    }, function() {
        $(this).removeClass('hover');
        $(this).addClass('hoverout');
        $('.fs__btnhover').removeClass('hover');
        $('.fs__btnhover').addClass('hoverout');
    });


    /******** EFFECTED START *********/

      jQuery.preloadImages = function()
       {
        for(var i = 0; i < arguments.length; i++)
        {
         jQuery("<img>").attr("src", arguments[ i ]);
        }
       };

      //указываем путь к изображению, которое нужно подгрузить
      $.preloadImages("/img/girl.jpg", "/img/thedart.jpg");



        // Generic function to set blur radius of $ele
        var setBlur = function(ele, radius) {
                ele.css({
                   "-webkit-filter": "blur("+radius+"px)",
                    "filter": "blur("+radius+"px)"
               });
           },

           // Generic function to tween blur radius
           tweenBlur = function(ele, startRadius, endRadius, speed) {

                currentRadius =  parseInt(ele.css('filter').slice(5, -3));

                if (currentRadius > 0 || currentRadius < 30) {
                  startRadius = currentRadius;
                }

                $({blurRadius: startRadius}).stop(true,true).animate({blurRadius: endRadius}, {
                    duration: speed,
                    easing: 'swing',
                    step: function() {

                        setBlur(ele, this.blurRadius);
                    },
                    complete: function() {
                        setBlur(ele, endRadius);
                   }
               });
            };



          $('.services__item').hover(function() {

            var flexy = $(this).data("flexy")
            var left   = $(this).offset().left;

              $('[role='+flexy+']').css('left', left+'px');
            if ($(this).index() > 4) {
              $('[role='+flexy+']').css('right', '0px');
              $('[role='+flexy+']').css('left', 'auto');
            }


            tweenBlur($('[role='+flexy+']') , 30, 0, 600);
            $('[role='+flexy+']').stop(true,true).animate({
              opacity: 1},
              600);

          }, function() {
            var flexy = $(this).data("flexy")
            tweenBlur($('[role='+flexy+']') , 0, 30, 600);
            $('[role='+flexy+']').stop(true,true).animate({
              opacity: 0},
              600);
          });


    /******** EFFECTED END *********/


    /******** SLIDER LICENSE START *********/
    if(document.getElementById('license__slider')) {
      const ps = new PerfectScrollbar('#license__slider', {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20,
        useBothWheelAxes: true
      });
    }
    /******** SLIDER LICENSE END *********/





    lightbox.option({
        albumLabel: "%1 из %2"
     })







    /******** SLIDER REVIEWS START *********/
  if(document.getElementById('reviews')) {

    $("#reviews").owlCarousel({
      items: 1,
      nav: false,
      loop: true,
      dots: true
    });

   }
    /******** SLIDER REVIEWS START *********/


    /******** SLIDER FULL START *********/
  if(document.getElementById('full1')) {

    $("#full1").owlCarousel({
      items: 1,
      nav: true,
      loop: true,
      dots: true,
      navText: [
        '<span class="icon-direction-prev"></span>',
        '<span class="icon-direction-next"></span>'
        ]
    });

   }
  if(document.getElementById('full2')) {

    $("#full2").owlCarousel({
      items: 1,
      nav: true,
      loop: true,
      dots: true,
      navText: [
        '<span class="icon-direction-prev"></span>',
        '<span class="icon-direction-next"></span>'
        ]
    });

   }
    /******** SLIDER FULL START *********/


    /******** GREED START *********/
    if ($('.grid__item').length > 0) {

      $('.grid__item').grid();

      $(window).resize(function(event) {
        $('.grid__item').grid();
      });

    }

    /******** GREED END *********/

    $('.inputlabel__field').focus(function(event) {
      $(this).parents('.inputlabel').addClass('focus');
        $(this).parents('.inputlabel').removeClass('focusout');

    });
    $('.inputlabel__field').focusout(function(event) {
      if ($(this).val() !== '') {
        $(this).parents('.inputlabel').addClass('focus');
      }
      else {
        $(this).parents('.inputlabel').addClass('focusout');
        $(this).parents('.inputlabel').removeClass('focus');
      }
    });



    /******** HEADER NEED SCROLL START *********/
    //Show social blocks in header for intertal page
      let headerOffset = $('.header').offset().top;
      let  gotTop = headerOffset;

      $(window).scroll(function(event) {

        if ($(window).scrollTop() > gotTop ) {
          $('.header').addClass('header--sticky');
          $('body').addClass('sticky');
        }
        else {
          $('.header').removeClass('header--sticky');
          $('body').removeClass('sticky');
        }
      });
    /******** HEADER NEED SCROLL END *********/

});




    // var Scrollbar = window.Scrollbar;

    // var mainscrollbar = Scrollbar.init(document.querySelector('#main-scrollbar'), {
    //   damping: 0.06,
    //   thumbMinSize: 96,
    //   continuousScrolling: false,
    //   alwaysShowTracks  : false
    // });
    // mainscrollbar.addListener((status, e) => {
    //   stickyHeader(status.offset.y);
    // });



    // function stickyHeader(y) {
    //   if (y > 0) {
    //     $('.header').addClass(' header--sticky')
    //     $('.header').css('top', y+'px');
    //   }
    //   else {
    //     $('.header').removeClass('header--sticky')
    //     $('.header').css('top', 0);

    //   }
    // }





        // var flexyElement = document.getElementById('rev-2');
        //     var rev2 = new RevealFx(flexyElement);

        //     function openFlexy() {
        //         rev2.reveal({
        //             isContentHidden:true,
        //             bgcolor: '#007C87',
        //             duration: 800,
        //             coverArea: 0,
        //             onCover: function(contentEl, revealerEl) {
        //                 contentEl.style.opacity = 1;
        //             }
        //         });
        //     }

        //     function closeFlexy() {
        //         rev2.reveal({
        //             isContentHidden:true,
        //             bgcolor: '#007C87',
        //             duration: 800,
        //             coverArea: 0,
        //             onCover: function(contentEl, revealerEl) {
        //                 contentEl.style.opacity = 0;
        //             }
        //         });
        //     }


        //       $('.services__item').hover(function() {

        //           // openFlexy()

        //       }, function() {
        //           // closeFlexy()
        //       });
