;(function( $ ){
  
  $.fn.grid = function(el) { 
    var firstElement = $(this).first();
    var minOffset = firstElement.offset().left;
    var count = 0;

    firstElement.attr('data-range', count);

    /*add range attr*/
    $(this).not(firstElement).each(function(index, el) {

      var left = $(el).offset().left;
      if (left === minOffset) count++;

      $(el).attr('data-range', count);  

    });


    /*set Max Height*/
    var maxRange = count;
    
    for (var i = 0; i < (maxRange+1); i++) {
      var group = $(this).filter('[data-range='+i+']');
      var maxH = 0;

      group.css('height', 'auto');

      $(group).each(function(index, el) {
        var h_block = $(el).height();
         if(h_block > maxH) {
            maxH = h_block;
         };
      });

      group.height(maxH)

    }
    return this; 
  };

})( jQuery );
