/* navigator.js s*/
$( document ).ready(function() {
    const nav        = $('.navigator');
    const wr1        = $('.navigator__wrapp-lvl1');
    const wr2        = $('.navigator__wrapp-lvl2');
    const header   = $('.navigator__header');
    const body      = $('#navigator__body-js');
    const card       = $('.navigator__card-js');
    const html       = $('html');
    let crumbs       = ['core'];
    let flagToggle  = false;

    function forDescktop(callback) {
        if ($(window).outerWidth() > 767) {
            callback();
        }
    }
    function forMobile(callback) {
        if ($(window).outerWidth() < 768) {
            callback();
        }
    }

    function reset () {
      $('.navigator__list-lvl1.show').removeClass('show');
      $('.navigator__list-lvl2.show').removeClass('show');
      card.removeClass('hide');
      wr2.removeClass('shown-third-level');

    }

    function close () {
     reset();
     $('.navigator__link-lvl1.active').removeClass('active')
     wr2.removeClass('show');
    }

    $('.navigator').click(function(e) {
                let target = $(e.target);


                if (target.is('.navigator__link--group')) {
                        let nameList = target.data('name-list');
                        let list           = $('[role='+nameList+']');

                        crumbs.push(nameList);

                        forMobile(function () {
                            wr1.toggleClass('show');
                            wr2.toggleClass('show');
                            list.addClass('show');
                        })

                        forDescktop(function() {
                            if (target.is('.active')) return false;

                            html.addClass('open-menu');
                            wr2.addClass('show');
                            reset();
                            list.addClass('show');

                            let navw = nav.width();
                            let width = $('.page__width-js').width() - navw - 26;
                            wr2.width(width);

                            let height = body.height();
                                 height += 83;
                            wr2.css({'min-height': height});

                            $('.navigator__link--group').removeClass('active');
                            target.addClass('active');
                        });

                        return false;
                }

                if (target.is('.navigator__link--innergroup')) {
                        let nameList         = target.data('name-list');
                        let list                   = $('[role='+nameList+']');
                        let currentName  = crumbs[crumbs.length-1]
                        let currentList      =  $('[role='+currentName+']');


                        forMobile(function(){
                          currentList.removeClass('show');
                        });

                        forDescktop(function(){
                          $('.navigator__list-lvl2.show').removeClass('show');

                          $('.navigator__link--innergroup').removeClass('active');
                          target.addClass('active')

                          wr2.addClass('shown-third-level');

                        });

                        list.addClass('show');
                        card.addClass('hide');
                        crumbs.push(nameList);
                        return false;
                }

                if (target.is('.navigator__back-link-js')) {
                        let currentName      = crumbs[crumbs.length-1];
                        let parentName       = crumbs[crumbs.length-2];

                        let currentList         = $('[role='+currentName+']');
                        let parent                = $('[role='+parentName+']');

                        if (parentName !== 'core') {
                            currentList.removeClass('show');
                            parent.addClass('show');
                        }
                        else {
                            wr1.toggleClass('show');
                            wr2.toggleClass('show');
                            currentList.removeClass('show');
                        }

                        crumbs.pop();


                        console.log(crumbs)

                }
    });

    $('html').click(function(event) {
                if ($(this).is('.open-menu') ) {

                    close();
                }
    });

    $('#navigator__header-js').click(function(event) {
        let parent = $(this).parents('.navigator');

        if (parent.is('.navigator--landing') && $(window).outerWidth() > 767) return false;

        $('#navigator__body-js').slideToggle('400');
        $(this).find('.navigator__btn span').toggleClass('icon-burger').toggleClass('icon-close');
    });

});
/* navigator.js e*/
